# RSS-reader

RSS (RDF Site Summary or Really Simple Syndication) is a web feed that allows users and applications to access updates to websites in a standardized, computer-readable format. RSS-reader, provided the RSS URL, converts the computer-readable format into a human friendly feeds.

## Features
- Previewing feeds of a RSS URL using command-line 
- print RSS feed as JSON if the option is selected
- ability to chose number of news previewed per given URL

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.
in the comman line, user provides a URL link and specifies the number of feeds they wish to see, and the news are demonstrated 
as follows:

## Documentation
For a documentation of this prgoram, please visit the websited mentioned [here](https://rss-parser.readthedocs.io/en/latest/).

## Installation 
```python
pip install rss-parser-celine-trial1
```

## License
For open source projects, say how it is licensed.

## Project status
Inital iteration is implemented and 3 more iterations are expected to be completed by the end of April 29th.
