.. _json_fromat_doc:

News Feed in JSON format
========================
If the user selects the option of being presented the feed in json format, depending on the --limit option, they will be shown that number of feeds in json format.

The JSON fomat implemented for the RSS-reader is as follows:
The feeds based on their relevence on the RSS file will be numbered starting from 1 and the output would appear in the followig form:

.. image:: /images/json_format.PNG
 :width: 250
 :alt: json format for news feed

with corresponding info about each news in front of the fields.

Below is an actual ouput of the news feed in JSON format is represented:


.. image:: /images/cmi_json.PNG
 :width: 650
 :alt: json format for news feed - CMI