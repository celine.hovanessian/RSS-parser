Notes for the Reviewers
=======================
Dear reviewers, during the implementation of this project, I faced a few vague points and complications that I will present in this section, some of them for the purpose of clarification.

pycodestyle errors
******************
In the output of the pycodestyle, there were few **too many blank lines** error. They were regarding the 2 blank lines I surrounded my module functions with, according the `pep-8 guideline <https://peps.python.org/pep-0008/#blank-lines>`_.

Future works
************
I will be improving this project every time I have time, as I have learned a lot from this project and I am still learning. My next step is going to implement test, as this step was not mandatory. Then I will be working on the implementation of the 6th iteration.