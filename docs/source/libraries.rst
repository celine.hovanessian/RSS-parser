Used Libraries in the Project
=============================

For the implementation of this project, a number of libraries have been made use of. The most important of them are as follows:
| These libraries are as follows:

| ``argparse`` for parsing the arguments of the CMI
| ``xml.etree.ElementTree`` for parsing the XML file of RSS into XML objects		    
| ``requests`` for fetching web pages
| ``json`` for converting the dictionary into json format
		     This library has used in the second approach for this goal, and is not in the initial implementation (in the commented section)
| ``logging`` for logging info/warning/error messages when the verbose option is set to on
| ``re`` for regular expression operations
| ``datetime`` and ``dateutil`` for the date format conversions
| ``textwrap`` for wrapping the text in 120 characters format
| ``pickle`` for data serialization during caching process
| ``reportlab`` for producing PDF documents
| ``BeautifulSoup`` for parsing HTML content into elements