.. trial documentation master file, created by
   sphinx-quickstart on Fri Apr 22 18:24:46 2022.

Welcome to RSS-reader documentation!
====================================
Welcome to the documentation of RSS-reader. 

RSS (RDF Site Summary or Really Simple Syndication) is a web feed that allows users and applications to access updates to websites in a standardized, computer-readable format. Subscribing to RSS feeds can allow a user to keep track of many different websites in a single news aggregator, which constantly monitor sites for new content, removing the need for the user to manually check them. News aggregators (or "RSS readers") can be built into a browser, installed on a desktop computer, or installed on a mobile device.[`Wikipedia <https://en.wikipedia.org/wiki/RSS>`_]

This RSS-reader comes in a command line interface format, and provided the RSS link of a webpage, will provide the RSS feeds of that webpage in two formats, including a JSON form. The user has the option to decide how many feeds they are willing to get and based on this option, they will be shown that many feeds.




.. toctree::
   :maxdepth: 2
   :caption: Contents:

   how_to_use
   json_format
   cached_feeds
   app_behavior
   libraries
   notes
