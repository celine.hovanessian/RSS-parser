.. _how_to_use:

How to use the RSS-reader
==========================
The RSS-reader is represented in CMI form, by running the program, user can enter the RSS of a webpage and the number of feeds they have selected as the option.

Below is demonstrated the help of the program

.. image:: /images/rss_help.PNG
 :width: 500
 :alt: RSS-reader help

As it is mentioned in the help of the program, user have multiple options as follows:

| ``help`` which displays the options, flags and arguments for the program.
| ``version`` displays the version in which the program comes in
 		     If this option is selected, only the version will be printed.
| ``json`` if selected, the news feed will be printed in json format
| ``verbose`` if selected, the user will be informed by the logs shown to them that how the program progresses
| ``limit`` the number of news feeds the user is willing to be shown. If not specified, all the feeds available on the RSS will be output.

And as argument, it is required to input an RSS URL.

| ``source`` the RSS URL which the user expects to see feeds from
| ``date`` the date specified by the user to be shown the feed from that date
| ``--to-html`` path to which the HTML file should be stored
| ``--to-pdf`` path to which the PDf file should be stored
| ``--colorize`` prints the output in colorized format

| If the distribution is installed, program can also be run using the rss_reader command, without needing the py command.

Below is illustrate an example of running the program with multiple options:

.. image:: /images/rss_example.PNG
 :alt: RSS-reader help
