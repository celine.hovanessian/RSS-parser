How RSS-reader Utility Works
=============================

-  If the user selects --version option as an argument, the version of the utility will be printed and the program will be ended.
-  If the user selects --verbose option, verbose will be printed in the stdout
-  If the user selects --colorize option, the output on stoud would be printed in colorful format.
-  If the user doesn't enter neither RSS URL nor --date, an error will be raised.
Otherwise, the behavior of the utility would be as it is illustrated in the diagram below:


.. image:: /images/Decision_Tree.jpg
 :alt: utility behavior diagram